// playing game

Vue.component("button-counter", {
    data() {
        return {
            count: 0
        }
    },
    props: [ "player", "score" ],
    methods: {
        countScore() {
            if(this.count < this.score) {
                this.count++;
            }
        }
    },
    template:`
        <button @click="countScore">
            <p>Player: {{ player }}</p>
            <p>Score: {{ count }}</p>
        </button>`
});

Vue.component("playing-game", {
    props: ["gameStart", "players", "score"],
    methods: {
        resetGame() {
            this.$emit('reset-game');
        }
    },
    template:`
    <div>
        <p>Total Players: {{ players.length }} / Winning Score: {{ score }}</p>
        <button-counter
        v-for="player in players"
        :key="player"
        :player="player"
        :score="score"
        ></button-counter>
        <div>
            <button @click="resetGame">reset</button>
        </div>
    </div>`
})

// setting game

Vue.component("player-list", {
    props: ["name"],
    template: `
    <li>
        {{ name }}
        <button @click="$emit('remove')">-</button>
    </li>
    `
})


Vue.component("setting-game", {
    data() {
        return {
            newPlayerName: "",
            players: [],
            winningScore: null,
        }
    },
    methods: {
        addNewPlayer() {
            this.players.push(this.newPlayerName);
            this.newPlayerName="";
        },
        playGame() {
            this.$emit('game-start', this.players, this.winningScore);
        }
    },
    template: `
    <form @submit.prevent="playGame">
        <form @submit.prevent="addNewPlayer">
              <p>STEP 1. Add new players!!</p>
              <input
              placeholder="e.g. Jackson"
              v-model="newPlayerName"
              >
              <button>+</button>  
        </form>
        <ul>
            <player-list
            v-for="(player, index) in players"
            :key="player"
            :name="player"
            @remove="players.splice(index, 1)"
            >
            </player-list>
        </ul>
        <p>STEP 2. Set the winning score!!</p>
        <select v-model="winningScore">
          <option disabled value="">Please set winning score</option>
          <option>5</option>
          <option>10</option>
          <option>15</option>
          <option>20</option>
          <option>25</option>
          <option>30</option>
        </select>
        <br>
        <button type="submit" :class="['btn-start']">start</button>
    </form>
    `
});

// connect to html file

new Vue({
    el: "#practice",
    data: {
        gameStart: false,
        showSettingGame: true,
        players: null,
        winningScore: null,
    },
    methods: {
        playGame(players, winningScore) {
            this.gameStart = true;
            this.showSettingGame = false;
            this.players = players;
            this.winningScore = winningScore;
        },
        resetGame() {
            this.gameStart = false;
            this.showSettingGame = true;
        }
    }
});
